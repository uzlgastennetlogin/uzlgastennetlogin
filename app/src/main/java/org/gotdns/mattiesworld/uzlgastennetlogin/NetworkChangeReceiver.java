package org.gotdns.mattiesworld.uzlgastennetlogin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import com.commonsware.cwac.wakeful.WakefulIntentService;


public class NetworkChangeReceiver extends BroadcastReceiver {
    private final String TAG = getClass().getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
        if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
            ConnectivityManager connectionManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfoWifi = connectionManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            String ssid;
            if (networkInfoWifi.isConnected()) {
                ssid = getWifiSSID(context);
            } else {
                ssid = null;
            }
            Intent serviceIntent = new Intent(context, LoginService.class);
            serviceIntent.putExtra("SSID", ssid);
            Log.d(TAG, "call service, ssid = " + ssid);
            WakefulIntentService.sendWakefulWork(context, serviceIntent);
        }
    }

    private String getWifiSSID(Context context) {
        WifiManager myWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = myWifiManager.getConnectionInfo();
        return wifiInfo.getSSID();
    }


}
