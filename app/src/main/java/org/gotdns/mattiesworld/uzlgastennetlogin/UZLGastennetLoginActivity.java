package org.gotdns.mattiesworld.uzlgastennetlogin;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import org.gotdns.mattiesworld.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UZLGastennetLoginActivity extends Activity {
    private final String TAG = getClass().getName();
    private final UZLGastennetLoginReceiver uzlGastennetLoginReceiver = new UZLGastennetLoginReceiver();
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    private TextView statusTextView;
    private TextView lastLoginTextView;
    private TextView lastCheckTextView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.preferences:
                startActivity(new Intent(UZLGastennetLoginActivity.this, Preferences.class));
                break;
            case R.id.about:
                startActivity(new Intent(UZLGastennetLoginActivity.this, AboutActivity.class));
                break;

        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Log.d(TAG, "registering receiver");
        this.registerReceiver(uzlGastennetLoginReceiver, new IntentFilter(LoginService.UZLGASTENNET_BROADCAST));
        initGUI();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        statusTextView.setText(savedInstanceState.getString("status"));
        lastLoginTextView.setText(savedInstanceState.getString("lastLogin"));
        lastCheckTextView.setText(savedInstanceState.getString("lastCheck"));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("status", statusTextView.getText().toString());
        outState.putString("lastLogin", lastLoginTextView.getText().toString());
        outState.putString("lastCheck", lastCheckTextView.getText().toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "unregistering receiver");
        this.unregisterReceiver(uzlGastennetLoginReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

/* setting a custom locale
        Locale locale = new Locale("ru");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
*/
        if (LoginService.getLastLoggedIn() != 0) {
            lastLoginTextView.setText(getResources().getString(R.string.act_lastLogin) + ": " + new SimpleDateFormat("HH:mm:ss").format(new Date(LoginService.getLastLoggedIn())));
        }
        if (LoginService.getLastCheck() != 0) {
            lastCheckTextView.setText(getResources().getString(R.string.act_lastCheck) + ": " + new SimpleDateFormat("HH:mm:ss").format(new Date(LoginService.getLastCheck())));
        }

        Intent fakeBroadcastIntent = new Intent(LoginService.UZLGASTENNET_BROADCAST);
        fakeBroadcastIntent.putExtra(LoginService.NEW_STATUS, LoginService.getLastStatus().toString());
        fakeBroadcastIntent.putExtra(LoginService.NEW_STATUS_EXTRA, LoginService.getLastStatusExtra());

        uzlGastennetLoginReceiver.onReceive(getApplicationContext(), fakeBroadcastIntent);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initGUI() {
        Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                Intent serviceIntent = new Intent(getApplicationContext(), LoginService.class);
                serviceIntent.putExtra("SSID", LoginService.UZLGUEST_SSID);
                getApplicationContext().startService(serviceIntent);
            }
        });
        Button checkButton = (Button) findViewById(R.id.checkButton);
        checkButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                Intent serviceIntent = new Intent(getApplicationContext(), LoginService.class);
                serviceIntent.putExtra("action", "checkOutside");
                getApplicationContext().startService(serviceIntent);
            }
        });
        findViewById(R.id.logoutButton).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent serviceIntent = new Intent(getApplicationContext(), LoginService.class);
                serviceIntent.putExtra("SSID", LoginService.UZLGUEST_SSID);
                serviceIntent.putExtra("action", "logout");
                getApplicationContext().startService(serviceIntent);
            }
        });
/*
        findViewById(R.id.disableWifiButton).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent serviceIntent = new Intent(getApplicationContext(), LoginService.class);
                serviceIntent.putExtra("action", "disableWifi");
                getApplicationContext().startService(serviceIntent);
                view.setEnabled(false);
            }
        });
*/
        statusTextView = (TextView) findViewById(R.id.outputTextView);
        lastLoginTextView = (TextView) findViewById(R.id.lastLoginTextView);
        lastCheckTextView = (TextView) findViewById(R.id.lastCheckTextView);
    }

    private class UZLGastennetLoginReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive");
            LoginService.STATUS status = LoginService.STATUS.valueOf(intent.getStringExtra(LoginService.NEW_STATUS));
            switch (status) {
                case INIT:
                    statusTextView.setText(getResources().getText(R.string.status_init));
                    break;
                case WIFI_UZLGUEST:
                    Log.d(TAG, "WIFI_UZLGUEST");
                    statusTextView.setText(getResources().getText(R.string.status_wifi_uzlguest));
                    findViewById(R.id.loginButton).setEnabled(true);
                    findViewById(R.id.logoutButton).setEnabled(true);
                    findViewById(R.id.checkButton).setEnabled(true);
//                    findViewById(R.id.disableWifiButton).setEnabled(true);
                    break;
                case WIFI_NOTCONNECTED:
                    statusTextView.setText(getResources().getText(R.string.status_wifi_notconnected));
                    findViewById(R.id.loginButton).setEnabled(false);
                    findViewById(R.id.logoutButton).setEnabled(false);
                    findViewById(R.id.checkButton).setEnabled(false);
//                    findViewById(R.id.disableWifiButton).setEnabled(false);
                    break;
                case WIFI_DISABLED:
                    statusTextView.setText(getResources().getText(R.string.status_wifi_disabled));
                    break;
                case WIFI_UNKNOWN:
                    statusTextView.setText(getResources().getText(R.string.status_wifi_unknown) + " (" + intent.getStringExtra(LoginService.NEW_STATUS_EXTRA) + ")");
                    findViewById(R.id.loginButton).setEnabled(false);
                    findViewById(R.id.logoutButton).setEnabled(false);
                    findViewById(R.id.checkButton).setEnabled(false);
                    break;
                case CHECK_CONNECTING:
                    statusTextView.setText(getResources().getString(R.string.act_checkOutside));
                    lastCheckTextView.setText(getResources().getString(R.string.act_lastCheck) + ": " + dateFormat.format(new Date(LoginService.getLastCheck())));
                     break;
                case CHECK_OK:
                    statusTextView.setText(getResources().getString(R.string.act_checkOutside_ok));
                    break;
                case CHECK_NOK:
                    statusTextView.setText(getResources().getString(R.string.act_checkOutside_nok));
                    break;
                case CHECK_FAILURE:
                    statusTextView.setText(getResources().getString(R.string.act_checkOutside_exc));
                    break;
                case PORTAL_CONNECTING:
                    statusTextView.setText(getResources().getString(R.string.status_portal_connecting));
                    break;
                case PORTAL_CONNECTED:
                    statusTextView.setText(getResources().getString(R.string.status_portal_connected));
                    break;
                case PORTAL_FAILURE:
                    statusTextView.setText(getResources().getString(R.string.status_portal_failure));
                    break;
                case LOGIN_CONNECTING:
                    statusTextView.setText(getResources().getString(R.string.status_login_connecting));
                    break;
                case LOGIN_LOGGEDIN:
                    statusTextView.setText(getResources().getString(R.string.status_login_loggedin));
                    lastLoginTextView.setText(getResources().getString(R.string.act_lastLogin) + ": " + dateFormat.format(new Date(LoginService.getLastLoggedIn()))
                            + " (" + intent.getStringExtra(LoginService.NEW_STATUS_EXTRA) + ")");
                    break;
                case LOGIN_FAILURE:
                case LOGIN_OTHER_ERROR:
                    statusTextView.setText(getResources().getString(R.string.status_login_fail) + " (" + intent.getStringExtra(LoginService.NEW_STATUS_EXTRA) + ")");
                    break;
                case LOGIN_EXPIRED:
                    break;
                case LOGOUT_CONNECTING:
                    statusTextView.setText(getResources().getString(R.string.status_logout_connecting));
                    break;
                case LOGOUT_SUCCESS:
                    statusTextView.setText(getResources().getString(R.string.status_logout_success));
                    break;
                case LOGOUT_FAILURE:
                    statusTextView.setText(getResources().getString(R.string.status_logout_failure) + " (" + intent.getStringExtra(LoginService.NEW_STATUS_EXTRA) + ")");
                    break;
            }
        }
    }
}