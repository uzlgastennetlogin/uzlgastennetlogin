package org.gotdns.mattiesworld.uzlgastennetlogin;

import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public abstract class HttpGetTask extends AsyncTask<String, Void, String> {
    private final String TAG = getClass().getName();

    private HttpClient httpClient;

    protected HttpGetTask(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    protected String doInBackground(String... params) {
        String url = params[0];
        HttpGet getReq = new HttpGet(url);
        Log.d(TAG, "HttpGetTask " + url);
        try {
            HttpResponse response = httpClient.execute(getReq);
            String responseBody = EntityUtils.toString(response.getEntity());
            Log.d(TAG, responseBody);
            return responseBody;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    abstract protected void onPostExecute(String result);

    public void executeSynchronous(String... params) {
        Log.d(TAG, "HttpGetTask executing synchronously!");
        onPreExecute();
        String result = doInBackground(params);
        onPostExecute(result);
    }
}