package org.gotdns.mattiesworld.uzlgastennetlogin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.http.AndroidHttpClient;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import com.commonsware.cwac.wakeful.WakefulIntentService;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.KeyStore;
import java.util.Timer;
import java.util.TimerTask;

public class LoginService extends WakefulIntentService {
    public static final String UZLGASTENNET_BROADCAST = "org.gotdns.mattiesworld.uzlgastennetlogin.UZLGASTENNET_BROADCAST";

    public static final String NEW_STATUS = "NEW_STATUS";
    public static final String NEW_STATUS_EXTRA = "NEW_STATUS_EXTRA";
    private static STATUS lastStatus = STATUS.INIT;
    private static String lastStatusExtra;
    private static String passwordDigest;
    private static long lastLoggedIn;
    private static long lastCheck;
    public static final String PORTAL_API_URL = "https://gastennetwerk.nexuzhealth.be:8001/index.php?zone=gastennetwerk_uzl&redirurl=http%3A%2F%2Fwww.tweakers.be%2F";
    private Timer autoCheckTimer;

    public LoginService() {
        super("LoginService");
    }

    public static enum STATUS {
        INIT,
        WIFI_NOTCONNECTED,
        WIFI_DISABLED,
        WIFI_UNKNOWN,
        WIFI_UZLGUEST,
        CHECK_CONNECTING,
        CHECK_OK,
        CHECK_NOK,
        CHECK_FAILURE,
        PORTAL_CONNECTING,
        PORTAL_CONNECTED,
        PORTAL_FAILURE,
        LOGIN_CONNECTING,
        LOGIN_LOGGEDIN,
        LOGIN_FAILURE,
        LOGIN_OTHER_ERROR,
        LOGIN_EXPIRED,
        LOGOUT_CONNECTING,
        LOGOUT_SUCCESS,
        LOGOUT_FAILURE
    }

    public static final String PORTAL_ERROR_NO_LOGIN = "error_logon_no-login";
    public static final String PORTAL_ERROR_NO_PASSWORD = "error_logon_no-password";
    public static final String PORTAL_ERROR_BAD_LOGIN_PASSWORD = "error_logon_bad-login-or-password";
    public static final String PORTAL_ERROR_MAX_CONN_REACHED = "error_logon_max-connection-reached";
    public static final String PORTAL_ERROR_LOGIN_EXPIRED = "error_logon_no-logindddddddddddddd";
    public static final String PORTAL_CODE_DISCONNECT_SUCCESS = "disconnect_success";

    private final String TAG = getClass().getName();

    private AndroidHttpClient httpClient;
    public static final String UZLGUEST_SSID = "UZLeuven-gast";

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            Log.d(TAG, "creating http client");
            httpClient = getNewHttpClient();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "error creating http client");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "shutting down http client");
        httpClient.close();
    }

/*
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (intent != null) handleIntent(intent);
        return START_STICKY;
    }
*/

    @Override
    protected void doWakefulWork(Intent intent) {
        Log.d(TAG, "onHandleIntent");
        final String action = intent.getStringExtra("action");
        if (action != null) {
            if ("logout".equals(action)) {
                invalidateTimer();
                logout();
            } else if ("checkOutside".equals(action)) {
                checkOutside();
            } else if ("disableWifi".equals(action)) {
                Log.d(TAG, "autodisablewifi feature has been deprecated");
//                disableWifi();
            }
        } else {
            invalidateTimer();
            String ssid = intent.getStringExtra("SSID");
            if (ssid == null) {
                broadcastStatus(STATUS.WIFI_NOTCONNECTED);
            } else if (ssid.contains(UZLGUEST_SSID)) {
                broadcastStatus(STATUS.WIFI_UZLGUEST);
                login();
            } else {
                broadcastStatus(STATUS.WIFI_UNKNOWN, ssid);
            }
        }
    }

/*
    private void disableWifi() {
        Log.d(TAG, "disabling wifi..");
        WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(false);
        broadcastStatus(STATUS.WIFI_DISABLED);
        Timer timer = new Timer("EnableWifi");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "enabling wifi..");
                WifiManager wifiManager = (WifiManager) LoginService.this.getSystemService(Context.WIFI_SERVICE);
                wifiManager.setWifiEnabled(true);
            }
        }, 5 * 60 * 1000);
    }
*/

    private void invalidateTimer() {
        if (autoCheckTimer == null) return;
        Log.d(TAG, "invalidate timer");
        autoCheckTimer.cancel();
        autoCheckTimer = null;
    }

    public static STATUS getLastStatus() {
        return lastStatus;
    }

    public static String getLastStatusExtra() {
        return lastStatusExtra;
    }

    public static String getPasswordDigest() {
        return passwordDigest;
    }

    public static long getLastLoggedIn() {
        return lastLoggedIn;
    }

    public static long getLastCheck() {
        return lastCheck;
    }

    private void broadcastStatus(STATUS newStatus) {
        broadcastStatus(newStatus, "");
    }

    private void broadcastStatus(STATUS newStatus, String extra) {
        Log.d(TAG, "broadcastStatus " + newStatus);
        lastStatus = newStatus;
        lastStatusExtra = extra;
        Intent broadcastIntent = new Intent(UZLGASTENNET_BROADCAST);
        broadcastIntent.putExtra(NEW_STATUS, newStatus.toString());
        broadcastIntent.putExtra(NEW_STATUS_EXTRA, extra);
        sendBroadcast(broadcastIntent, null);
    }

    private void login() {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final String user = preferences.getString("username", "");
        final String pwd = preferences.getString("password", "");

        Log.d(TAG, "logging in " + user);
        new HttpPostTask(httpClient, PORTAL_API_URL) {
            @Override
            protected void onPreExecute() {
                broadcastStatus(STATUS.LOGIN_CONNECTING);
            }

            public void onPostExecute(String responseBody) {
                if (responseBody == null) {
                    broadcastStatus(STATUS.PORTAL_FAILURE);
                } else {
                    try {
                        Log.d(TAG, responseBody);
                        //{"error":{"code":"error_disconnect_internal","value":null}}
                        JSONObject response = new JSONObject(responseBody);
                        if (!response.isNull("error")) {
                            String errCode = response.getJSONObject("error").getString("code");
                            if (PORTAL_ERROR_NO_LOGIN.equals(errCode)) {
                                broadcastStatus(STATUS.LOGIN_FAILURE, PORTAL_ERROR_NO_LOGIN);
                            } else if (PORTAL_ERROR_NO_PASSWORD.equals(errCode)) {
                                broadcastStatus(STATUS.LOGIN_FAILURE, PORTAL_ERROR_NO_PASSWORD);
                            } else if (PORTAL_ERROR_BAD_LOGIN_PASSWORD.equals(errCode)) {
                                broadcastStatus(STATUS.LOGIN_FAILURE, PORTAL_ERROR_BAD_LOGIN_PASSWORD);
                            } else if (PORTAL_ERROR_MAX_CONN_REACHED.equals(errCode)) {
                                broadcastStatus(STATUS.LOGIN_FAILURE, PORTAL_ERROR_MAX_CONN_REACHED);
                            } else {
                                broadcastStatus(STATUS.LOGIN_OTHER_ERROR, errCode);
                            }
                        } else if (!response.isNull("user")) {
                            //{"step":"FEEDBACK","type":"CONNECT","user":{"login":{"value":"*****"},"passwordDigest":{"value":"**********"},"ipAddress":{"value":"172.23.0.115"},"profile":{"value":"guest"},"services":{"value":"Full_Access"},"autoDisconnect":{"value":true},"schedule":{"value":[{"begin":{"day":"monday","hour":"00","min":"00"},"end":{"day":"sunday","hour":"24","min":"00"}}]},"validity":{"value":"0"},"incommingVlan":{"value":"800"},"incommingZone":{"value":"gastennetwerk-draadloos"},"universalTime":{"value":1311697700},"requestedURL":{"value":"http:\/\/www.google.com"}}}
                            passwordDigest = response.getJSONObject("user").getJSONObject("passwordDigest").getString("value");
                            String ipAddress = response.getJSONObject("user").getJSONObject("ipAddress").getString("value");
                            Log.i(TAG, "ip = " + ipAddress);
                            Log.i(TAG, "digest = " + passwordDigest);
                            lastLoggedIn = System.currentTimeMillis();
                            broadcastStatus(STATUS.LOGIN_LOGGEDIN, ipAddress);
                            if (preferences.getBoolean("autoCheck", true)) {
                                Log.d(TAG, "autocheck internet access");
                                checkOutside();
                            }
                            if (preferences.getBoolean("autoCheckTimer", false)) {
                                Log.d(TAG, "autoCheckTimer has been deprecated");
//                                        autoCheckTimer = new Timer("checkOutsideTimer");
//                                        final int delay = Integer.parseInt(preferences.getString("autoCheckInterval", "15")) * 60000;
//                                        autoCheckTimer.scheduleAtFixedRate(new CheckOutSideTimerTask(), delay, delay);
                            }
                        } else {
                            Log.w(TAG, "unhandled response");
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "error parsing json response", e);
                        broadcastStatus(STATUS.PORTAL_FAILURE, "error parsing json response");
                    }
                }
            }
        }.executeSynchronous("auth_user", user, "auth_pass", pwd, "accept", "Log in");
    }

    private void logout() {
        Log.d(TAG, "logout");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final String user = preferences.getString("username", "");

        Log.d(TAG, "logging out " + user);
        new HttpPostTask(httpClient, PORTAL_API_URL) {
            @Override
            protected void onPreExecute() {
                broadcastStatus(STATUS.LOGOUT_CONNECTING);
            }

            public void onPostExecute(String responseBody) {
                if (responseBody == null) {
                    broadcastStatus(STATUS.PORTAL_FAILURE);
                } else {
                    try {
                        JSONObject response = new JSONObject(responseBody);
                        if (!response.isNull("step") && "LOGON".equals(response.getString("step"))) {
                            if (LoginService.PORTAL_CODE_DISCONNECT_SUCCESS.equals(response.getJSONObject("info").getString("code"))) {
                                broadcastStatus(STATUS.LOGOUT_SUCCESS);
                            } else {
                                broadcastStatus(STATUS.LOGOUT_FAILURE, response.getJSONObject("info").getString("code"));
                            }
                        } else {
                            broadcastStatus(STATUS.LOGOUT_FAILURE, response.getJSONObject("info").getString("code"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        broadcastStatus(STATUS.PORTAL_FAILURE, "error parsing json response");
                    }
                }
            }
        }.executeSynchronous("action", "disconnect", "login", user, "password_digest", LoginService.getPasswordDigest());
    }

    private void checkOutside() {
        Log.d(TAG, "checkOutside");
        CheckOutside checkOutside = new CheckOutside();
        checkOutside.executeSynchronous();
    }

    private class CheckOutside extends AsyncTask<Void, Void, Boolean> {
        private final String TAG = getClass().getName();
        private String lastCheckBody;

        @Override
        protected void onPreExecute() {
            lastCheck = System.currentTimeMillis();
            broadcastStatus(STATUS.CHECK_CONNECTING);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Log.d(TAG, "check worker");
            HttpGet getReq = new HttpGet("http://mattiesworld.gotdns.org/UZLGastennetLogin/");
            AndroidHttpClient httpClient = null;
            try {
                httpClient = LoginService.getNewHttpClient();
                HttpResponse response = httpClient.execute(getReq);
                lastCheckBody = EntityUtils.toString(response.getEntity());
                Log.d(TAG, lastCheckBody);
                return "ok".equals(lastCheckBody);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (httpClient != null) {
                    httpClient.close();
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result == null) {
                Log.d(TAG, "something weird happened");
                broadcastStatus(STATUS.CHECK_FAILURE, "proxy problem");
                if (((UZLGastennetLoginApp)getApplicationContext()).isAutoDisableWifi()) {
                    Log.d(TAG, "autodisablewifi feature has been deprecated");
//                    disableWifi();
                }
                return;
            }
            if (result)
                broadcastStatus(STATUS.CHECK_OK);
            else
                broadcastStatus(STATUS.CHECK_NOK, "check failed");
        }

        public void executeSynchronous() {
            Log.d(TAG, "CheckOutside executing synchronously!");
            onPreExecute();
            boolean ok = doInBackground();
            onPostExecute(ok);
        }

    }

    protected class CheckOutSideTimerTask extends TimerTask {
        @Override
        public void run() {
            Log.d(TAG, "CheckOutSideTimerTask timer triggered");
            Intent serviceIntent = new Intent(getApplicationContext(), LoginService.class);
            serviceIntent.putExtra("action", "checkOutside");
            getApplicationContext().startService(serviceIntent);
        }
    }

    public static AndroidHttpClient getNewHttpClient() throws Exception {
        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(null, null);

        SSLSocketFactory sf = new AcceptAllHostsSSLSocketFactory(trustStore);
        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        AndroidHttpClient client = AndroidHttpClient.newInstance("UZLGastennetLoginApp");
        client.getConnectionManager().getSchemeRegistry().register(new Scheme("https", sf, 443));
        return client;
    }
}
