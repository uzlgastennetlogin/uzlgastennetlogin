package org.gotdns.mattiesworld.uzlgastennetlogin;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import org.gotdns.mattiesworld.R;

public class UZLGastLoginWidgetProvider extends AppWidgetProvider {
    private final String TAG = getClass().getName();

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.d(TAG, "onEnabled");
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.d(TAG, "onUpdate");
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        updateWidget(context, LoginService.getLastStatus());
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (LoginService.UZLGASTENNET_BROADCAST.equals(intent.getAction())) {
            Log.d(TAG, "loginservice broadcast received");
            LoginService.STATUS status = LoginService.STATUS.valueOf(intent.getStringExtra(LoginService.NEW_STATUS));
            updateWidget(context, status);
        }
    }

    private void updateWidget(Context context, LoginService.STATUS status) {
        int newImage = 0;
        switch (status) {
            case INIT:
                newImage = R.drawable.widget_init;
                break;
            case WIFI_NOTCONNECTED:
                newImage = R.drawable.widget_wifi_notconnected;
                break;
            case WIFI_UNKNOWN:
                newImage = R.drawable.widget_wifi_unknown;
                break;
            case WIFI_UZLGUEST:
                newImage = R.drawable.widget_wifi_uzl;
                break;
            case CHECK_CONNECTING:
            case CHECK_OK:
            case CHECK_NOK:
                break;
            case PORTAL_CONNECTING:
                break;
            case PORTAL_CONNECTED:
                newImage = R.drawable.widget_portal;
                break;
            case PORTAL_FAILURE:
                newImage = R.drawable.widget_portal_fail;
                break;
            case LOGIN_CONNECTING:
                break;
            case LOGIN_LOGGEDIN:
                newImage = R.drawable.widget_login;
                break;
            case LOGIN_FAILURE:
            case LOGIN_OTHER_ERROR:
            case LOGIN_EXPIRED:
            case CHECK_FAILURE:
                newImage = R.drawable.widget_login_fail;
                break;
            case LOGOUT_CONNECTING:
            case LOGOUT_SUCCESS:
            case LOGOUT_FAILURE:
                break;
        }
        if (newImage != 0) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.uzlgastlogin_appwidget);
            remoteViews.setImageViewResource(R.id.imageView, newImage);
            remoteViews.setOnClickPendingIntent(R.id.imageView, PendingIntent.getActivity(context, 0,
                    new Intent(context, UZLGastennetLoginActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));

            ComponentName thisWidget = new ComponentName(context, UZLGastLoginWidgetProvider.class);
            AppWidgetManager.getInstance(context).updateAppWidget(thisWidget, remoteViews);
        }
    }
}
