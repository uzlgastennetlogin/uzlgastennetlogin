package org.gotdns.mattiesworld.uzlgastennetlogin;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * <p/>
 * Created by IntelliJ IDEA.
 * User: msweer1
 * Date: 9/08/11
 * Time: 20:07
 */
public class UZLGastennetLoginApp extends Application {
    private final String TAG = getClass().getName();

    private int notificationLevel = -1;
    private boolean autoDisableWifi;

    @Override
    public void onCreate() {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        notificationLevel = Integer.parseInt(preferences.getString("notificationLevel",
                        String.valueOf(NotificationReceiver.NOTIFICATION.ERROR.ordinal())));
        autoDisableWifi = preferences.getBoolean("autoDisableWifi", false);
    }

    public int getNotificationLevel() {
        return notificationLevel;
    }

    public void setNotificationLevel(int notificationLevel) {
        Log.d(TAG, "setNotificationLevel " + notificationLevel);
        this.notificationLevel = notificationLevel;
    }

    public boolean isAutoDisableWifi() {
        return autoDisableWifi;
    }

    public void setAutoDisableWifi(boolean autoDisableWifi) {
        this.autoDisableWifi = autoDisableWifi;
    }
}
