package org.gotdns.mattiesworld.uzlgastennetlogin;

import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class HttpPostTask extends AsyncTask<String, Void, String> {
    private final String TAG = getClass().getName();

    private HttpClient httpClient;
    private String url;

    protected HttpPostTask(HttpClient httpClient, String url) {
        this.httpClient = httpClient;
        this.url = url;
    }

    @Override
    protected String doInBackground(String... params) {
        HttpPost postReq = new HttpPost(url);
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        for (int i = 0; i < params.length / 2; i++) {
            nameValuePairs.add(new BasicNameValuePair(params[i * 2], params[i * 2 + 1]));
        }
        Log.d(TAG, "HttpPostTask " + url);
        try {
            postReq.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpClient.execute(postReq);
            String responseBody = EntityUtils.toString(response.getEntity());
            Log.d(TAG, responseBody);
            return responseBody;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    abstract protected void onPostExecute(String result);

    public void executeSynchronous(String... params) {
        Log.d(TAG, "HttpPostTask executing synchronously!");
        onPreExecute();
        String result = doInBackground(params);
        onPostExecute(result);
    }
}