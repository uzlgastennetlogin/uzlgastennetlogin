package org.gotdns.mattiesworld.uzlgastennetlogin;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import org.gotdns.mattiesworld.R;

public class AboutActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        TextView textView = (TextView) findViewById(R.id.aboutTextView);
        String version = "unknown";
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = "v" + packageInfo.versionName + " (" + packageInfo.versionCode + ")";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        textView.setText(Html.fromHtml("<h3>UZLGastennetLogin app</h3>" +
                "<textspan align=\"center\">You are running version " + version + "</textspan>" +
                "<p>To apply for the beta, join this <a href='https://plus.google.com/u/0/communities/116183147378077954800'>G+ community</a>.<br>" +
                "<a href='http://mattiesworld.gotdns.org/weblog/2013/10/03/uzlgastennetlogin-v0-6-3-stable-release/'>This blog post</a> explains beta access in more detail.</p>" +
                "<p>Visit the <a href='http://mattiesworld.gotdns.org/weblog/category/coding-excursions/uzlgastennetlogin/'>project blog</a>.</p>"
        ));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
}