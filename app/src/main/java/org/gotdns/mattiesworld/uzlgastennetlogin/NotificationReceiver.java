package org.gotdns.mattiesworld.uzlgastennetlogin;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;

import org.gotdns.mattiesworld.R;

public class NotificationReceiver extends BroadcastReceiver {
    private final static String TAG = NotificationReceiver.class.getName();

    public static enum NOTIFICATION {
        NONE,
        ERROR,
        INFO,
        VERBOSE
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "loginservice broadcast received");
        int currentNotificationLevel = ((UZLGastennetLoginApp)context.getApplicationContext()).getNotificationLevel();
        if (currentNotificationLevel == NOTIFICATION.NONE.ordinal()) return;
        LoginService.STATUS status = LoginService.STATUS.valueOf(intent.getStringExtra(LoginService.NEW_STATUS));
        int notificationType = NOTIFICATION.NONE.ordinal();
        CharSequence ticker = null;
        CharSequence content = null;
        String extra = intent.getStringExtra(LoginService.NEW_STATUS_EXTRA);
        switch (status) {
            case WIFI_NOTCONNECTED:
                notificationType = NOTIFICATION.VERBOSE.ordinal();
                ticker = context.getResources().getText(R.string.status_wifi_notconnected);
                break;
            case WIFI_DISABLED:
                notificationType = NOTIFICATION.VERBOSE.ordinal();
                ticker = context.getResources().getText(R.string.status_wifi_disabled);
                break;
            case WIFI_UNKNOWN:
                notificationType = NOTIFICATION.VERBOSE.ordinal();
                ticker = context.getResources().getText(R.string.status_wifi_unknown);
                break;
            case WIFI_UZLGUEST:
                notificationType = NOTIFICATION.VERBOSE.ordinal();
                ticker = context.getResources().getText(R.string.status_wifi_uzlguest);
                break;
            case PORTAL_FAILURE:
            case LOGIN_FAILURE:
            case LOGIN_OTHER_ERROR:
            case LOGIN_EXPIRED:
            case LOGOUT_FAILURE:
            case CHECK_FAILURE:
            case CHECK_NOK:
                notificationType = NOTIFICATION.ERROR.ordinal();
                ticker = context.getResources().getString(R.string.notification_error);
                content = extra;
                break;
            case CHECK_OK:
            case LOGIN_LOGGEDIN:
                clearAllNotifications(context);
                notificationType = NOTIFICATION.INFO.ordinal();
                ticker = context.getResources().getString(R.string.notification_success);
                break;
        }
        if (notificationType == NOTIFICATION.NONE.ordinal() || notificationType > currentNotificationLevel) return;
        sendNotification(context, notificationType, ticker, content);
    }


    private void sendNotification(Context context, int notificationType, CharSequence ticker, CharSequence content) {
        Log.d(TAG, "Sending notification " + ticker);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int icon = R.drawable.notification;
        long when = System.currentTimeMillis();
        CharSequence contentTitle = "UZLGastennetLogin";

        Intent notificationIntent = new Intent(context, UZLGastennetLoginActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, Intent.FLAG_ACTIVITY_NEW_TASK);

        Notification notification = new Notification(icon, ticker, when);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.sound = Uri.parse(PreferenceManager.getDefaultSharedPreferences(context).getString("notificationSound", "content://settings/system/notification_sound"));
        notification.setLatestEventInfo(context, contentTitle, content == null ? ticker : content, contentIntent);
        notificationManager.cancelAll();
        notificationManager.notify(notificationType, notification);
    }

    private void clearNotifications(Context context, int notificationType) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationType);
    }

    private void clearAllNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

}
