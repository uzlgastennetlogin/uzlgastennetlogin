package org.gotdns.mattiesworld.uzlgastennetlogin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import org.gotdns.mattiesworld.R;

public class Preferences extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceChangeListener {
    private final String TAG = getClass().getName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
//        getPreferenceScreen().findPreference("autoCheckInterval").setOnPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
         Log.d(TAG, "onSharedPreferenceChanged" + s);
         if ("notificationLevel".equals(s)) {
             ((UZLGastennetLoginApp)getApplicationContext()).setNotificationLevel(Integer.valueOf(sharedPreferences.getString(s, "0")));
         } else if ("password".equals(s)) {
             if (!"".equals(sharedPreferences.getString("password", "")) && !"".equals(sharedPreferences.getString("username", ""))) {
                 requestLogin();
             }
         } else if ("autoDisableWifi".equals(s)) {
             ((UZLGastennetLoginApp) getApplicationContext()).setAutoDisableWifi(sharedPreferences.getBoolean(s, false));
         }
     }

    private void requestLogin() {
        Intent serviceIntent = new Intent(getApplicationContext(), LoginService.class);
        serviceIntent.putExtra("SSID", LoginService.UZLGUEST_SSID);
        getApplicationContext().startService(serviceIntent);
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        return newValue.toString().matches("\\d+") && Integer.parseInt(newValue.toString()) > 0;
    }
}
